import React, { Component } from "react";
import { Router, Switch, Route } from "react-router-dom";

import Cohost from "./Cohost/Cohost"
import Project from "./Project/Project";
import Release from "./Release/Release";
import Home from "./Home/Home";
import history from './history';
import Login from "./LoginInterFace/Login";

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route path="/" exact component={Login} />
                    <Route path="/Home" exact component={Home} />
                    <Route path="/Cohost" component={Cohost} />
                    <Route path="/Project" component={Project} />
                    <Route path="/Release" component={Release} />
                </Switch>
            </Router>
        )
    }
}
